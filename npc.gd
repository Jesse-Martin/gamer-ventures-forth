extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var g = 100
export var jump_speed = 2000
var velocity = Vector2.ZERO
export var  walk_speed = 100
var left = true
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func turn():
	left = !left

func _physics_process(delta):
	if left:
		velocity.x = walk_speed
	else:
		velocity.x = -walk_speed
	velocity.y +=  g * delta
	var l =move_and_slide(velocity, Vector2.UP)
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	turn() # Replace with function body.
